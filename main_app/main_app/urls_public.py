from django.contrib import admin
from django.urls import path, include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="API for Stock management application",
        default_version='v1',
        description=''
    ),
    public=True,
    permission_classes=(permissions.AllowAny,)
)

urlpatterns = [
    path('api/', include('base.urls_public')),
    path('api/', include('stocks.urls_public')),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-view'),
    path('admin/', admin.site.urls)
]
