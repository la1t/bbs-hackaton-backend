from django.db import models


class Stock(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()

    lifetime_min = models.DurationField(blank=True, null=True)
    lifetime_max = models.DurationField(blank=True, null=True)
    subscriptions = models.ManyToManyField('base.Subscription', blank=True)
    balance_min = models.IntegerField(blank=True, null=True)
    balance_max = models.IntegerField(blank=True, null=True)
    connected_services = models.ManyToManyField('base.Service', blank=True, related_name='+')

    services = models.ManyToManyField('base.Service')
    dealers = models.ManyToManyField('base.Dealer')

    def __str__(self):
        return self.name
