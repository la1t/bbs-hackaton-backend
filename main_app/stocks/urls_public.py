from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('stocks', views.StockViewSet)

urlpatterns = router.urls
