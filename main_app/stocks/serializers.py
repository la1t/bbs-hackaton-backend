from rest_framework import serializers
from .models import Stock
from base.models import Subscription, Service, Dealer


class StockSerializer(serializers.HyperlinkedModelSerializer):
    subscriptions = serializers.PrimaryKeyRelatedField(many=True, queryset=Subscription.objects.all(), required=False)
    connected_services = serializers.PrimaryKeyRelatedField(many=True, queryset=Service.objects.all(), required=False)
    services = serializers.PrimaryKeyRelatedField(many=True, queryset=Service.objects.all())
    dealers = serializers.PrimaryKeyRelatedField(many=True, queryset=Dealer.objects.all())

    def is_valid(self, raise_exception=False):
        super().is_valid(raise_exception=False)

        if not (self.initial_data['lifetime_min'] or self.initial_data['lifetime_max'] or
                self.initial_data['subscriptions'] or self.initial_data['balance_min'] or
                self.initial_data['balance_max'] or self.initial_data['connected_services']):
            self._errors['conditions'] = ['Должно быть указано хотя бы одно условие для подключения тарифа']

        if self._errors and raise_exception:
            raise serializers.ValidationError(self.errors)

    class Meta:
        model = Stock
        fields = ['id', 'url', 'name', 'description',
                  'lifetime_min', 'lifetime_max', 'subscriptions', 'balance_min',
                  'balance_max', 'connected_services', 'services', 'dealers']
