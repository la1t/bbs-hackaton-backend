import datetime
from decimal import Decimal
from django.test import TestCase
from ..models import Client
from base.models import Subscription, Service
from ..factories import ClientFactory


class ClientFactoryTest(TestCase):
    databases = ['default']

    def test_generate_msisdn(self):
        msisdn = ClientFactory().generate_msisdn()
        self.assertEqual(len(msisdn), 10)
        self.assertEqual(msisdn[0], '9')
        self.assertTrue(msisdn.isdigit())

    def test_generate_subscription(self):
        Subscription.objects.create(name='sub#1')
        Subscription.objects.create(name='sub#2')
        self.assertIn(ClientFactory().generate_subscription(), Subscription.objects.all())

    def test_generate_additional_services(self):
        # just smoke
        Service.objects.create(name='service#1')
        Service.objects.create(name='service#2')
        Service.objects.create(name='service#3')
        ClientFactory().generate_additional_services()

    def test_generate_connection_date(self):
        factory = ClientFactory(year_delta=1)
        conn_date = factory.generate_connection_date()
        now = datetime.datetime.now()
        year_ago = now - datetime.timedelta(days=365)
        self.assertIsInstance(conn_date, datetime.datetime)
        self.assertLessEqual(conn_date, now)
        self.assertLessEqual(year_ago, conn_date)

    def test_generate_balance(self):
        factory = ClientFactory()
        balance = factory.generate_balance()
        self.assertIsInstance(balance, Decimal)
        balance_tuple = balance.as_tuple()
        self.assertLessEqual(len(balance_tuple.digits), 10)
        self.assertEqual(balance_tuple.exponent, -2)

    def test_creating(self):
        Service.objects.create(name='service#1')
        Service.objects.create(name='service#2')
        Service.objects.create(name='service#3')
        Subscription.objects.create(name='sub#1')
        Subscription.objects.create(name='sub#2')
        factory = ClientFactory()
        instance = factory()
        self.assertIsInstance(instance, Client)
        self.assertTrue(instance.additional_services.count())




