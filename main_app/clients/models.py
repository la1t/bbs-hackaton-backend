from django.db import models
from .validators import validate_msisdn


class Client(models.Model):
    msisdn = models.CharField(max_length=10, validators=[validate_msisdn], unique=True, primary_key=True)
    subscription = models.ForeignKey('base.Subscription', on_delete=models.SET_NULL, blank=True, null=True)
    additional_services = models.ManyToManyField('base.Service')
    connection_date = models.DateTimeField()
    balance = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.msisdn
