import random
import string
import datetime
from decimal import Decimal
from .models import Client
from base.models import Subscription, Service


def get_random_date(start, end):
    delta = end - start
    return start + datetime.timedelta(random.randint(0, delta.days))


class ClientFactory(object):
    def generate_msisdn(self):
        msisdn = '9%s' % ''.join(random.choices(string.digits, k=9))
        try:
            Client.objects.get(msisdn=msisdn)
        except Client.DoesNotExist:
            return msisdn
        return self.generate_msisdn()

    def generate_subscription(self):
        count_subscriptions = Subscription.objects.count()
        assert count_subscriptions
        return Subscription.objects.all()[random.randint(0, count_subscriptions-1)]

    def generate_additional_services(self):
        count_services = Service.objects.count()
        assert count_services
        generate_count = random.randint(1, count_services)
        return Service.objects.order_by('?')[:generate_count]

    def generate_connection_date(self):
        now = datetime.datetime.now()
        ago = now - datetime.timedelta(365*self.year_delta)
        return get_random_date(ago, now)

    def generate_balance(self):
        fraction = ''.join(random.choices(string.digits, k=2))

        whole_num = random.randint(0, 8)
        if whole_num != 0:
            whole = ''.join(random.choices(string.digits, k=whole_num))
        else:
            whole = '0'

        sign = '-' if random.getrandbits(1) else ''
        decimal_str = sign + '%s.%s' % (whole, fraction)
        return Decimal(decimal_str)

    def __init__(self, year_delta=2):
        self.year_delta = year_delta

    def __call__(self):
        client = Client.objects.create(
            msisdn=self.generate_msisdn(),
            subscription=self.generate_subscription(),
            balance=self.generate_balance(),
            connection_date=self.generate_connection_date()
        )
        for service in self.generate_additional_services():
            client.additional_services.add(service)
        return client
