from django.core.exceptions import ValidationError


def validate_msisdn(val):
    if len(val) != 10:
        raise ValidationError('MSISDN must contains 10 symbols')
    if val[0] != '9':
        raise ValidationError('MSISDN must starts from "9"')
