from django.core.management.base import BaseCommand, CommandError
from clients.factories import ClientFactory


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('num_clients', type=int)

    def handle(self, *args, **options):
        if options['num_clients'] < 1:
            raise CommandError('num_clients must be > 1')

        factory = ClientFactory()
        for _ in range(options['num_clients']):
            client = factory()
            self.stdout.write('Created: %s' % client)
        self.stdout.write(self.style.SUCCESS('Successfully created %s clients' % options['num_clients']))
