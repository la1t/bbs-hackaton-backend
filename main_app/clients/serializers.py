from rest_framework import serializers
from .models import Client
from base.models import Service, Subscription


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    subscription = serializers.PrimaryKeyRelatedField(queryset=Subscription.objects.all())
    additional_services = serializers.PrimaryKeyRelatedField(many=True, queryset=Service.objects.all())

    class Meta:
        model = Client

        fields = ['url', 'msisdn', 'subscription', 'additional_services', 'connection_date', 'balance']
