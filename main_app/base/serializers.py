from rest_framework import serializers
from .models import Service, Subscription, Dealer


class ServiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Service
        fields = ['id', 'url', 'name']


class SubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Subscription
        fields = ['id', 'url', 'name']


class DealerSerializer(serializers.HyperlinkedModelSerializer):
    stock_set = serializers.PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta:
        model = Dealer
        fields = ['id', 'url', 'name', 'stock_set']
