from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('dealers', views.DealerViewSet)
router.register('services', views.ServiceViewSet)
router.register('subscriptions', views.SubscriptionViewSet)

urlpatterns = router.urls
