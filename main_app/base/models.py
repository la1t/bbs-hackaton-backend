from django.db import models


class Service(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Subscription(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Dealer(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name
