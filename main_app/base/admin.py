from django.contrib import admin
from .models import Subscription, Service, Dealer


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    pass


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Dealer)
class DealerAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
