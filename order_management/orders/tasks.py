import random
from time import sleep
from order_management.celery import app
from .models import Order


@app.task
def perform_order(id_):
    order = Order.objects.get(id=id_)
    delay = random.randint(0, 50) / 10
    sleep(delay)
    if random.randint(0, 9):
        # client = Client.objects.get(msisdn=order.client_msisdn)
        # stock = Stock.objects.get(id=order.stock_id)
        # for service in stock.services.all():
        #     client.additional_services.add(service)
        order.status = Order.SUCCESS
    else:
        order.status = Order.FAILED
    order.save()
    return 'Order #%s : %s (delay %s s)' % (
        order.id,
        'SUCCESS' if order.status == order.SUCCESS else 'FAILED',
        delay
    )
