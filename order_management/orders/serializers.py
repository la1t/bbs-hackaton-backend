import requests
from decimal import Decimal
from django.utils import timezone
from rest_framework import serializers
from rest_framework import status
from django.utils.dateparse import parse_duration, parse_datetime
from .models import Order


def calculate_lifetime(dt):
    return timezone.now() - dt


def validate_conditions(client, stock):
    errors = []

    client_conn_date = parse_datetime(client['connection_date'])
    client_lifetime = calculate_lifetime(client_conn_date)

    if stock['lifetime_max'] and client_lifetime >= parse_duration(stock['lifetime_max']):
        errors.append('lifetime_max')

    if stock['lifetime_min'] and client_lifetime <= parse_duration(stock['lifetime_min']):
        errors.append('lifetime_min')

    for subscription_id in stock['subscriptions']:
        if subscription_id == client['subscription']:
            break
    else:
        errors.append('subscriptions')

    if stock['balance_max'] and Decimal(client['balance']) >= Decimal(stock['balance_max']):
        errors.append('balance_max')

    if stock['balance_min'] and Decimal(client['balance']) <= Decimal(stock['balance_min']):
        errors.append('balance_min')

    for service_id in stock['connected_services']:
        if service_id not in client['additional_services']:
            errors.append('connected_services')

    return errors


class OrderSerializer(serializers.HyperlinkedModelSerializer):

    def is_valid(self, raise_exception=False):
        super().is_valid(raise_exception=False)

        if not self._errors.get('client_msisdn'):
            response = requests.get('http://mainprivate/api/clients/%s/' % self.initial_data['client_msisdn'])
            if response.status_code == status.HTTP_404_NOT_FOUND:
                self._errors['client_msisdn'] = ['Клиента с указанным msisdn не существует']
            else:
                client = response.json()
                print(client)

        if not self._errors.get('dealer_id'):
            response = requests.get('http://mainpublic/api/dealers/%s/' % self.initial_data['dealer_id'])
            if response.status_code == status.HTTP_404_NOT_FOUND:
                self._errors['dealer_id'] = ['Партрера с указанным id не существует']
            else:
                dealer = response.json()
                print(dealer)

        if not self._errors.get('stock_id'):
            response = requests.get('http://mainpublic/api/stocks/%s/' % self.initial_data['stock_id'])
            if response.status_code == status.HTTP_404_NOT_FOUND:
                self._errors['stock_id'] = ['Акции с указанным id не существует']
            else:
                stock = response.json()
                print(stock)

        if not self._errors.get('dealer_id') and not self._errors.get('stock_id'):
            if stock['id'] not in dealer['stock_set']:
                self._errors['dealer_id'] = ['Партнер с указанным id не распространяет эту услугу']

        # TODO!
        # if not self._errors.get('client_msisdn'):
        #     stocked_services = Service.objects.stocked()
        #     for service in stocked_services:
        #         if service in client.additional_services.all():
        #             self._errors['client_msisdn'] = ['Клиент уже имеет подключенные акции']
        #             break

        if not self._errors.get('client_msisdn') and not self._serrors.get('stock_id'):
            for service_id in client['additional_services']:
                if service_id in stock['services']:
                    self._errors['client_msisdn'] = ['Клиент уже подключил эту услугу']

        if not self._errors.get('client_msisdn') and not self._errors.get('stock_id'):
            condition_errors = validate_conditions(client, stock)
            if condition_errors:
                self._errors['conditions'] = condition_errors

        if self._errors and raise_exception:
            raise serializers.ValidationError(self.errors)

    class Meta:
        model = Order
        fields = ['id', 'url', 'client_msisdn',
                  'dealer_id', 'stock_id', 'status', 'created_at']
        read_only_fields = ['status']
