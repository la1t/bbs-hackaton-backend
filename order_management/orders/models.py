from django.db import models


class Order(models.Model):
    PENDING = 1
    FAILED = 2
    SUCCESS = 3

    STATUSES = [
        (PENDING, 'PENDING'),
        (FAILED, 'FAILED'),
        (SUCCESS, 'SUCCESS')
    ]

    client_msisdn = models.CharField(max_length=10)
    dealer_id = models.PositiveIntegerField()
    stock_id = models.PositiveIntegerField()
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=PENDING)
    created_at = models.DateTimeField(auto_now_add=True)
