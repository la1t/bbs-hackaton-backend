from django.urls import path
from . import views

urlpatterns = [
    path('orders/', views.OrderCreate.as_view(), name='order-create'),
    path('orders/validate/', views.OrderValidate.as_view(), name='order-validate'),
    path('orders/<int:pk>/', views.OrderDetail.as_view(), name='order-detail'),
]
